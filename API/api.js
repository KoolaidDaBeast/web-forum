const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const app = express();

const port = process.env.PORT || 5000;
const CloudManager = require('./manager');

//Setting Cross-Origin Headers
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");

    next();
});

//Setting express to use body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

//Connect to the database
mongoose.connect(process.env.MONGO_URL);

/*
    THREADS
*/
app.get('/api/forum/thread/:threadId', async(req, res) => {
    const { key, pageId } = req.query;
    const { threadId } = req.params;

    if ((await CloudManager.getAccountPermissions(key)).includes('CAN_VIEW_POSTS') == false) {
        return res.json({
            success: false,
            permission: false,
            message: 'You do not have permission to do this'
        });
    }

    await CloudManager.incrementThreadViews(threadId);

    return res.json({
        success: true,
        result: await CloudManager.getPosts(threadId)
    });
});

app.get('/api/forum/threads', async(req, res) => {
    const { key, catId, topId } = req.query;

    if ((await CloudManager.getAccountPermissions(key)).includes('CAN_VIEW_POSTS') == false) {
        return res.json({
            success: false,
            permission: false,
            message: 'You do not have permission to do this'
        });
    }

    return res.json({
        success: true,
        result: await CloudManager.getThreads(catId, topId)
    });
});

app.post('/api/forum/thread', async(req, res) => {
    const { category, topic, title, postBody, key } = req.body;

    if ((await CloudManager.getAccountPermissions(key)).includes('CAN_POST') == false) {
        return res.json({
            success: false,
            message: 'You do not have permission to do this'
        });
    }

    if (title == '') {
        return res.json({
            success: false,
            message: 'Please add a thread title'
        });
    }

    if (postBody.toLowerCase().includes('<script>') || postBody.toLowerCase().includes('</script>')) {
        return res.json({
            success: false,
            message: 'Please remove the script tags from the post.'
        });
    }

    if (await CloudManager.categoryIdExists(category) == false) {
        return res.json({
            success: false,
            message: 'This category does not exist.'
        });
    }

    const threadId = await CloudManager.addThread(key, category, topic, title);

    if (threadId != null) {
        await CloudManager.addPost(key, threadId, postBody);

        return res.json({
            success: true,
            message: 'You have successfully created the thread!',
            url: `/forum/thread/${threadId}`
        });
    }

    return res.json({
        success: false,
        message: 'Failed to create the thread'
    });
});


/*
    CATEGORIES
*/
app.get('/api/forum/category', async(req, res) => {
    const { key } = req.query;

    if ((await CloudManager.getAccountPermissions(key)).includes('CAN_VIEW_POSTS') == false) {
        return res.json({
            success: false,
            permission: false,
            message: 'You do not have permission to do this'
        });
    }

    return res.json({
        success: true,
        result: await CloudManager.getCategories()
    });
});

app.get('/api/forum/category/:name', async(req, res) => {
    const { key } = req.query;
    const name = req.params.name;

    if ((await CloudManager.getAccountPermissions(key)).includes('CAN_VIEW_POSTS') == false) {
        return res.json({
            success: false,
            permission: false,
            message: 'You do not have permission to do this'
        });
    }

    return res.json({
        success: true,
        result: await CloudManager.getCategoryData(name)
    });
});

app.post('/api/forum/category', async(req, res) => {
    const { name, key } = req.body;

    if ((await CloudManager.getAccountPermissions(key)).includes('CAN_MANAGE_CATEGORY') == false) {
        return res.json({
            success: false,
            message: 'You do not have permission to do this'
        });
    }

    if (name.includes('/')) {
        return res.json({
            success: false,
            message: 'Invalid character / used.'
        });
    }

    if (await CloudManager.categoryExists(name)) {
        return res.json({
            success: false,
            message: 'This category already exist.'
        });
    }

    if (await CloudManager.addCategory(name)) {
        return res.json({
            success: true,
            message: 'Category has been succesfully created.'
        });
    }

    return res.json({
        success: false,
        message: 'Failed to create the category.'
    });
});

app.delete('/api/forum/category', async(req, res) => {
    const { name, key } = req.body;

    if ((await CloudManager.getAccountPermissions(key)).includes('CAN_MANAGE_CATEGORY') == false) {
        return res.json({
            success: false,
            message: 'You do not have permission to do this'
        });
    }

    if (await CloudManager.categoryExists(name) == false) {
        return res.json({
            success: false,
            message: 'This category does not exist.'
        });
    }

    if (await CloudManager.deleteCategory(name)) {
        return res.json({
            success: true,
            message: 'Category has been succesfully removed.'
        });
    }

    return res.json({
        success: false,
        message: 'Failed to delete the category.'
    });
});

app.post('/api/forum/category/topic', async(req, res) => {
    const { name, topic, key } = req.body;

    if ((await CloudManager.getAccountPermissions(key)).includes('CAN_MANAGE_CATEGORY') == false) {
        return res.json({
            success: false,
            message: 'You do not have permission to do this'
        });
    }

    if (name.includes('/') || topic.includes('/')) {
        return res.json({
            success: false,
            message: 'Invalid character / used.'
        });
    }

    if (await CloudManager.categoryExists(name) == false) {
        return res.json({
            success: false,
            message: 'This category does not exist'
        });
    }

    if (await CloudManager.addCategoryTopic(name, topic)) {
        return res.json({
            success: true,
            message: 'Topic ' + topic + ' has been added to the category successfully.',
            topic: topic
        });
    }

    return res.json({
        success: false,
        message: 'Failed to add the topic.'
    });
});

app.put('/api/forum/category/topic', async(req, res) => {
    const { name, topic, topicId, key } = req.body;

    if ((await CloudManager.getAccountPermissions(key)).includes('CAN_MANAGE_CATEGORY') == false) {
        return res.json({
            success: false,
            message: 'You do not have permission to do this'
        });
    }

    if (name.includes('/') || topic.includes('/')) {
        return res.json({
            success: false,
            message: 'Invalid character / used.'
        });
    }

    if (await CloudManager.categoryExists(name) == false) {
        return res.json({
            success: false,
            message: 'This category does not exist'
        });
    }

    if (await CloudManager.renameCategoryTopic(name, topicId, topic)) {
        return res.json({
            success: true,
            message: 'Topic has been renamed.',
            topic: topic
        });
    }

    return res.json({
        success: false,
        message: 'Failed to rename the topic.'
    });
});

app.delete('/api/forum/category/topic', async(req, res) => {
    const { name, id, key } = req.body;

    if ((await CloudManager.getAccountPermissions(key)).includes('CAN_MANAGE_CATEGORY') == false) {
        return res.json({
            success: false,
            message: 'You do not have permission to do this'
        });
    }

    if (await CloudManager.categoryExists(name) == false) {
        return res.json({
            success: false,
            message: 'This category does not exist'
        });
    }

    if (await CloudManager.deleteCategoryTopic(name, id)) {
        return res.json({
            success: true,
            message: 'Topic has been deleted.'
        });
    }

    return res.json({
        success: false,
        message: 'Failed to delete the topic.'
    });
});

/*
    ACCOUNTS
*/
app.post('/api/account/auth', async(req, res) => {
    const { username, password } = req.body;

    if (await CloudManager.accountExists(username) == false) {
        return res.json({
            success: false,
            message: 'Email doesnt exist'
        });
    }

    if (await CloudManager.authenicateAccount(username, password)) {
        return res.json({
            success: true,
            message: 'You have logged in successfully',
            key: await CloudManager.getApiKey(username),
            image: await CloudManager.getProfileImage(username)
        });
    }

    return res.json({
        success: false,
        message: 'Invalid username or password.'
    });
});

app.post('/api/account/register', async(req, res) => {
    const { displayName, email, password, confirmPassword, profileImage } = req.body;

    //Validate input fields
    if (displayName == '' || email == '') {
        return res.json({
            success: false,
            message: 'Please fill in all the fields.'
        });
    }

    if (!email.includes('@') || !email.includes('.')) {
        return res.json({
            success: false,
            message: 'Please provide a valid email address.'
        });
    }

    if (password.length < 7) {
        return res.json({
            success: false,
            message: 'Passwords must be greater than 7 chars long.'
        });
    }

    if (password != confirmPassword) {
        return res.json({
            success: false,
            message: 'Passwords do not match'
        });
    }

    if (await CloudManager.accountExists(email)) {
        return res.json({
            success: false,
            message: 'Email already in use!'
        });
    }

    if (await CloudManager.registerAccount(displayName, email, password, profileImage)) {
        return res.json({
            success: true,
            message: 'Account has been created!'
        });
    }

    return res.json({
        success: false,
        message: 'Failed to create the account.'
    });
});



//Error 404
app.get('*', (req, res) => {
    res.json({ status: false });
});

//Web Listener\\
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});