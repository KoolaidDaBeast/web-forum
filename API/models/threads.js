const mongoose = require('mongoose');

module.exports = mongoose.model('Threads', new mongoose.Schema({
    threadId: String,
    categoryId: String,
    topicId: String,
    title: String,
    owner: String,
    created_at: String,
    settings: String
}));