const mongoose = require('mongoose');

module.exports = mongoose.model('Posts', new mongoose.Schema({
    threadId: String,
    body: String,
    email: String,
    created_at: String
}));